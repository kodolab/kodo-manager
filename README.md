Improvements:
==================

Problematiche da risolvere:
---------------------------

1) Rapida condivisione delle configurazioni.
2) Ridondanza delle variabili nei file docker-compose 
3) uniformare docker e docker swarm 


(1)  configurazioni.
----------------------------------------------

Ogni utenete deve configurarsi localmente solo:
1) path del repository
2) override delle variabili
3) variabili locali

4) override del alias gruppo docker
5) alias per il lancio del comando












--------------------






Package kodo_manager

script di avvio `km`

`km -c conf.py`


File di configurazione (Envronments)
====================================


./conf/dev.env

./conf/prod.env

./conf/stage.env

Istruzioni installazione
====================================

**Installare pacchetto**

`pip install -e git+git@gitlab.com:kodolab/kodo-manager.git@master#egg=koto_manager` 


**Copiarsi in locale la docker machine condivisa e settare config.json**

1. `cd ~/.docker/machine/machines`

2. `tar zxvf ~/<Download>/<docker-machine>.tgz` 

3. `vi ~/.docker/machine/machines/<docker-machine>/config.json`

4. `ESC :%s/<utente>/<tuoutente>/`

5. `ESC :wq`

**Scaricare .kodo-manager e metterlo nella propria Home**

1. `tar zxvf ~/kodo_manager.tgz .`

2. `echo 'source ~/.kodo_manager/kodorc.sh'  >> .zshrc` 

3. `vi .kodo_manager/yg/conf.py`

4. replace path di progetto e variabili di sviluppo

    ```    
    vi .kodo_manager/yg/conf/dev.env
    vi .kodo_manager/yg/conf/stage.env
    vi .kodo_manager/yg/conf/prod.env
    ```
    
5. km (lancia kodo manager)

6. yg ( km -c ~/.kodo_manager/yg/conf.py )


**Scaricarsi pacchetti legati ai servizi (es proxy)**

es. `git@gitlab.com:yougardener/yg-proxy.git`



    



