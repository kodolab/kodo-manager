import os

from setuptools import setup


INSTALL_REQUIRES = [
    "docker>=3.5.1",
    "pyyaml>=3.13",
    "click==7.0",
    "python-dotenv>=0.9.1",
    "dockerpty==0.4.1",
    "python-jenkins==1.5.0"
]

def main():
    setup(
        name='kodo_manager',
        version='1.0',
        description='A wrapper for manage multiple docker-compose projects',
        author='Paolo Leggio',
        author_email='paolo@digitalmonkeys.it',
        scripts=['bin/km'],
        packages=['kodo_manager'],
        # test_suite='your.module.tests',
        install_requires=INSTALL_REQUIRES,
    )


if __name__ == "__main__":
    main()
