import unittest
import sys, os

SCRIPT_PATH = os.path.dirname(os.path.abspath(__file__))
ROOT_PATH = os.path.abspath(os.path.join(SCRIPT_PATH, '..'))
sys.path.insert(0, ROOT_PATH + '/.pylib/lib/python3.6/site-packages/')
sys.path.insert(0, ROOT_PATH + '/.pylib')

from kod.config import ConfigManager
from kod import env_manager
from conf import CLI_CONF, FE_PATH


class TestEnvManager(unittest.TestCase):

    def test_with_env(self):
        self.assertEqual('FOO' in os.environ, False)

        with env_manager.with_envs({'FOO':'BAR'}):
            self.assertEqual(os.environ['FOO'], 'BAR')

        self.assertEqual('FOO' in os.environ, False)
