import unittest
import sys, os

SCRIPT_PATH = os.path.dirname(os.path.abspath(__file__))
ROOT_PATH = os.path.abspath(os.path.join(SCRIPT_PATH, '..'))
sys.path.insert(0, ROOT_PATH + '/.pylib/lib/python3.6/site-packages/')
sys.path.insert(0, ROOT_PATH + '/.pylib')

from kod.config import ConfigManager
from conf import CLI_CONF, FE_PATH


class TestConfigManager(unittest.TestCase):
    def test_load_conf(self):
        cm = ConfigManager(CLI_CONF)

        self.assertEqual(cm._config, CLI_CONF)

    def test_get_steps(self):
        cm = ConfigManager(CLI_CONF)
        self.assertEqual(cm.steps(), ['fe_base', 'fe'])

    def test_get_compose_files(self):
        cm = ConfigManager(CLI_CONF)
        self.assertEqual(cm.get_compose_files(None, 'fe'),
                         [
                             '{}/.DOCKER/docker-compose-base.yml'.format(FE_PATH)
                             , '{}/.DOCKER/docker-compose-build.yml'.format(FE_PATH)
                          ])
