FROM python:3.7

COPY . /code
WORKDIR /code
RUN pip install --upgrade pip \
&& pip install -e . 
CMD /bin/bash
