import click
from click.core import _maybe_show_deprecated_notice
from contextlib import nullcontext
import dockerpty
import docker
import pprint
from kodo_manager import env_manager
from kodo_manager.click_extension import ClickAliasedGroup
from kodo_manager.docker_manager import copy_to_container


class AllowExtraArgsCommands(click.Command):
    allow_extra_args = True
    ignore_unknown_options = True
    allow_interspersed_args = False

    def invoke(self, ctx):
        """Given a context, this invokes the attached callback (if it exists)
                in the right way.
                """
        _maybe_show_deprecated_notice(self)
        if self.callback is not None:
            return ctx.invoke(self.callback, *ctx.args, **ctx.params)


def echo(cmd, container):
    click.secho('Exec command [', nl=False)
    click.secho(' '.join(cmd), nl=False, fg='bright_green')
    click.secho('] in ', nl=False)
    click.secho(container, nl=True, fg='bright_green')


@click.command(cls=AllowExtraArgsCommands)
@click.pass_context
def run_docker_command(ctx, *extra_args):
    # print('run_docker_command',extra_args)
    command_name = ctx.parent.invoked_subcommand
    command_spec = ctx.obj['CONFIG'].get_docker_command_spec(command_name)


    if not command_spec:
        raise Exception('invalid command')

    print('command_spec',command_spec)

    service = command_spec['service']
    action = command_spec['action']
    allow_extra_args = command_spec.get('allow_extra_args',False)
    pre_action = command_spec.get('pre_action')

    dgroup = ctx.obj['CONFIG'].get_docker_group_name(ctx.obj['ENV'])
    if ctx.obj['CONFIG'].get_docker_machine(ctx.obj['ENV']):
        docker_machine = ctx.obj['CONFIG'].get_docker_machine(ctx.obj['ENV'])

        with ctx.obj['DOCKER_MANAGER'].with_docker_machine(docker_machine) as docker_client:
            click.secho('####### Docker [{}] ##########'.format(docker_machine), fg='green')

            service_name = '{}_{}'.format(dgroup, service)
            try:
                service = None
                services = docker_client.services.list(filters={'name': service_name})
                if len(services) == 1:
                    service = services[0]
                elif len(services) > 1:
                    for s in services:
                        if s.name == service_name:
                            service = s
                    if service is None:
                        raise Exception('Multiple services found')
                else:
                    raise Exception('No service found')
            except:
                click.secho('Docker service [{}] Not Found'.format(service_name), fg='red')
                click.secho('Try to deploy without --update options'.format(), fg='yellow')
                ctx.exit()

            # print(service)
            # todo: I service possono avere piu di un task??
            # task = service.tasks()[0]
            # pprint.pprint(service.tasks())
            task = None
            for t in service.tasks():
                if t.get('Status',{}).get('State', None) == 'running':
                    task = t

            if task is None:
                raise Exception('No task running')

            containerId = task.get('Status',{}).get('ContainerStatus', {}).get('ContainerID')
            if not containerId:
                raise Exception('ContainerId not found')


            #detect docker Node to interact for command and define WithFunction
            node_with_service_running = docker_client.nodes.get(task['NodeID'])
            docker_machine_with_service_running = node_with_service_running.attrs.get('Description',{}).get('Hostname')
            
            if (docker_machine_with_service_running != docker_machine):
                click.secho('####### Switch Docker Node to [{}] ##########'.format(docker_machine_with_service_running), fg='green')
                withFunc = ctx.obj['DOCKER_MANAGER'].with_docker_machine(docker_machine_with_service_running)
            else:
                withFunc = nullcontext(docker_client)


            with withFunc as docker_with_running_container:
                
                # todo: Sistemare questo blocco fatto al volo per fare upload di un file prema di eseguire un comando
                if pre_action:
                    pre_action_type = pre_action.get('type', None)

                    if pre_action_type == 'upload':
                        dst_path = pre_action.get('path', None)
                        assert dst_path
                        extra_args_l = list(extra_args)
                        try:
                            file_path = extra_args_l.pop()
                            extra_args = tuple(extra_args_l)
                        except:
                            raise Exception('Missing file path')
                        copy_to_container(docker_with_running_container, containerId,file_path,dst_path )
                    else:
                        raise Exception('Uncknow type for pre_action: {}'.format(pre_action))





                # print('containerId',containerId)
                # container = docker_client.containers.get(containerId)

                # print('container',container)

                # (exit_code, output) = container.exec_run('bash',tty=True, detach=True)
                # print('(exit_code, output)',(exit_code, output))

                if isinstance(action, list):
                    cmd = action
                else:
                    cmd = action.split()

                if allow_extra_args:
                    cmd += extra_args
                print('cmd:',cmd)

                echo(cmd, containerId)
                dockerpty.exec_command(docker_with_running_container.api, containerId, cmd)
    else:
        client = docker.from_env()
        container_name = '{}-{}'.format(dgroup, service)


        candidate_containers = client.containers.list(filters={
            'name':container_name,
            'status': 'running'
        })
        if len(candidate_containers) == 0:
            # raise Exception('Container not found')
            click.secho(f'Container "{container_name}" not found!', fg='red')
            ctx.abort()

        container = candidate_containers[0]

        cmd = action.split()

        if allow_extra_args:
            cmd += extra_args

        echo(cmd, container_name)
        dockerpty.exec_command(client.api, container.id, cmd)



class DockerCommandGroupWrapper(ClickAliasedGroup):

    def add_docker_commands(self, ctx):
        # print('add_docker_commands',ctx.obj['CONFIG'].get_docker_command_keys())

        for command_name in ctx.obj['CONFIG'].get_docker_command_keys():
            self.add_command(run_docker_command, command_name)



    def make_context(self, info_name, args, parent=None, **extra):
        initial_args = args
        args = [x for x in args if x != '--help']

        initial_no_args_is_help = self.no_args_is_help
        initial_invoke_without_command = self.invoke_without_command
        self.no_args_is_help = False
        self.invoke_without_command = True
        ctx = super(DockerCommandGroupWrapper, self).make_context(info_name, args, parent, **extra)
        self.no_args_is_help = initial_no_args_is_help
        self.invoke_without_command = initial_invoke_without_command

        ctx.obj['ENV'] = parent.invoked_subcommand
        ctx.obj['JENKINS_SETUP'] = 'jenkins' in args


        self.add_docker_commands(ctx)


        with ctx.scope(cleanup=False):
            args = self.parse_args(ctx, initial_args)


        return ctx

    def invoke(self, ctx):
        """
        When i invoke the command configure in the scope the correct environment

        First create the environment files used in the docker-compose and after load the setup environment

        :param ctx:
        :return:
        """

        with env_manager.with_env_files(ctx.obj['ENV']):
            with env_manager.load_env(ctx.obj['ENV'], None) as environ:
                    return super(DockerCommandGroupWrapper, self).invoke(ctx)
                # try:
                # except click.UsageError as e:
                #     click.echo(ctx.get_help(), color=ctx.color)
                #     ctx.exit()