from click.types import Choice
from click import core, Group, pass_context
import inspect
from click.core import join_options, string_types
from click.exceptions import MissingParameter, _join_param_hints
from click.globals import get_current_context


class CustomMissingParameter(MissingParameter):
    def format_message(self):
        if self.param_hint is not None:
            param_hint = self.param_hint
        elif self.param is not None:
            param_hint = self.param.get_error_hint(self.ctx)
        else:
            param_hint = None
        param_hint = _join_param_hints(param_hint)

        param_type = self.param_type
        if param_type is None and self.param is not None:
            param_type = self.param.param_type_name

        msg = self.message
        if self.param is not None:
            msg_extra = self.param.type.get_missing_message(self.param, self.ctx)
            if msg_extra:
                if msg:
                    msg += '.  ' + msg_extra
                else:
                    msg = msg_extra

        return 'Missing %s%s%s%s' % (
            param_type,
            param_hint and ' %s' % param_hint or '',
            msg and '.  ' or '.',
            msg or '',
        )


class ServiceChoice(Choice):
    choices = None

    def __init__(self, case_sensitive=True):

        self.case_sensitive = case_sensitive

    def __repr__(self):
        return 'ServiceChoice'

    def get_choices(self, ctx=None):
        # print('get_choices:',ctx.obj.get('JENKINS_MODE'))
        if ctx.obj.get('JENKINS_MODE'):
            return ['all']
        self.choices = ctx.obj['CONFIG'].steps()
        return self.choices

    def get_metavar(self, param, ctx):
        return '[%s]' % '|'.join(self.get_choices(ctx))

    def convert(self, value, param, ctx):
        # Exact match
        choices = self.get_choices(ctx=ctx)
        if value in choices:
            return value

        # Match through normalization and case sensitivity
        # first do token_normalize_func, then lowercase
        # preserve original `value` to produce an accurate message in
        # `self.fail`
        normed_value = value
        normed_choices = choices

        if ctx is not None and \
                        ctx.token_normalize_func is not None:
            normed_value = ctx.token_normalize_func(value)
            normed_choices = [ctx.token_normalize_func(choice) for choice in
                              self.choices]

        if not self.case_sensitive:
            normed_value = normed_value.lower()
            normed_choices = [choice.lower() for choice in normed_choices]

        if normed_value in normed_choices:
            return normed_value

        self.fail('invalid choice: %s. (choose from %s)' %
                  (value, ', '.join(choices)), param, ctx)

    def get_missing_message(self, param, ctx):
        return 'Choose from:\n\t%s.' % ',\n\t'.join(self.get_choices(ctx=ctx))
        # return 'Choose correct service'


class ServicesOption(core.Option):
    def __init__(self, *args, **kwargs):
        kwargs['type'] = ServiceChoice()
        # self.type = ServiceChoice()
        super(ServicesOption, self).__init__(*args, **kwargs)

        # print('self.type',self.type)

    def get_default(self, ctx):
        steps = ctx.obj['CONFIG'].steps()
        if steps:
            return steps[len(steps) - 1]

        return None

    def full_process_value(self, ctx, value):
        value = self.process_value(ctx, value)

        if value is None and not ctx.resilient_parsing:
            value = self.get_default(ctx)

        if self.required and self.value_is_missing(value):
            raise CustomMissingParameter(ctx=ctx, param=self)

        return value

    def get_help_record(self, ctx):
        if self.hidden:
            return
        any_prefix_is_slash = []

        def _write_opts(opts):
            rv, any_slashes = join_options(opts)
            if any_slashes:
                any_prefix_is_slash[:] = [True]
            if not self.is_flag and not self.count:
                rv += ' ' + self.make_metavar(ctx)
            return rv

        rv = [_write_opts(self.opts)]
        if self.secondary_opts:
            rv.append(_write_opts(self.secondary_opts))

        help = self.help or ''
        extra = []
        if self.show_envvar:
            envvar = self.envvar
            if envvar is None:
                if self.allow_from_autoenv and \
                                ctx.auto_envvar_prefix is not None:
                    envvar = '%s_%s' % (ctx.auto_envvar_prefix, self.name.upper())
            if envvar is not None:
                extra.append('env var: %s' % (
                    ', '.join('%s' % d for d in envvar)
                    if isinstance(envvar, (list, tuple))
                    else envvar,))
        if self.default is not None and self.show_default:
            if isinstance(self.show_default, string_types):
                default_string = '({})'.format(self.show_default)
            elif isinstance(self.default, (list, tuple)):
                default_string = ', '.join('%s' % d for d in self.default)
            elif inspect.isfunction(self.default):
                default_string = "(dynamic)"
            else:
                default_string = self.default
            extra.append('default: {}'.format(default_string))

        if self.required:
            extra.append('required')
        if extra:
            help = '%s[%s]' % (help and help + '  ' or '', '; '.join(extra))

        return ((any_prefix_is_slash and '; ' or ' / ').join(rv), help)

    # @property
    # def metavar(self):
    #     # return []
    #     return ['aa','bb', 'cc']

    # def __init__(self,  case_sensitive=True):
    #     # self.choices = choices
    #     self.case_sensitive = case_sensitive
    #
    # @property
    # # @pass_context
    # def choices(self, ctx=None):
    #     print('ctx', ctx)
    #     # raise Exception('HAH')
    #     return ['aa','bb', 'cc']

    def make_metavar(self, ctx):
        if self.metavar is not None:
            return self.metavar
        metavar = self.type.get_metavar(self, ctx)
        if metavar is None:
            metavar = self.type.name.upper()
        if self.nargs != 1:
            metavar += '...'
        return metavar


class ClickAliasedGroup(Group):
    def __init__(self, *args, **kwargs):
        super(ClickAliasedGroup, self).__init__(*args, **kwargs)
        self._commands = {}
        self._aliases = {}

    def command(self, *args, **kwargs):
        aliases = kwargs.pop('aliases', [])
        decorator = super(ClickAliasedGroup, self).command(*args, **kwargs)
        if not aliases:
            return decorator

        def _decorator(f):
            cmd = decorator(f)
            if aliases:
                self._commands[cmd.name] = aliases
                for alias in aliases:
                    self._aliases[alias] = cmd.name
            return cmd

        return _decorator

    def group(self, *args, **kwargs):
        aliases = kwargs.pop('aliases', [])
        decorator = super(ClickAliasedGroup, self).group(*args, **kwargs)
        if not aliases:
            return decorator

        def _decorator(f):
            cmd = decorator(f)
            if aliases:
                self._commands[cmd.name] = aliases
                for alias in aliases:
                    self._aliases[alias] = cmd.name
            return cmd

        return _decorator

    def get_command(self, ctx, cmd_name):
        if cmd_name in self._aliases:
            cmd_name = self._aliases[cmd_name]
        command = super(ClickAliasedGroup, self).get_command(ctx, cmd_name)
        if command:
            return command

    def format_commands(self, ctx, formatter):
        rows = []
        for sub_command in self.list_commands(ctx):
            cmd = self.get_command(ctx, sub_command)
            if cmd is None:
                continue
            if hasattr(cmd, 'hidden') and cmd.hidden:
                continue
            if sub_command in self._commands:
                aliases = ','.join(sorted(self._commands[sub_command]))
                sub_command = '{0} or [{1}]'.format(sub_command, aliases)
            cmd_help = cmd.short_help or ''
            rows.append((sub_command, cmd_help))
        if rows:
            with formatter.section('Commands'):
                formatter.write_dl(rows)