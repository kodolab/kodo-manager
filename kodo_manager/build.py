import click
import os
from kodo_manager import utils, env_manager
import itertools




@click.pass_context
def push_step(ctx, env, step):
    compose_files = ctx.obj['CONFIG'].get_compose_files(env, step)
    print('push step',compose_files, env, step)

    utils.docker_login()

    if not compose_files:
        return
    compose_file_args = utils.compose_files_to_docker_command_args(compose_files)

    build_env_vars = env_manager.get_dict_envs(ctx.obj['CONFIG'].get_build_args(env, step))
    build_env_vars_args = [['--build-arg', '{}={}'.format(k, v)] for (k, v) in build_env_vars.items()]
    build_env_vars_args = list(itertools.chain(*build_env_vars_args))

    # Crate command
    args = ['docker compose']
    args += compose_file_args
    # args += ['--log-level','DEBUG']
    args += ['push']

    res = utils.run(args)
    if res != 0:
        click.secho('Error during push images  for env: {} step:{}'.format(env, step), fg='red')
        ctx.abort()





@click.pass_context
def build_step(ctx, env, step):

    service_links = ctx.obj['CONFIG'].get_linked_services(step)
    compose_files = ctx.obj['CONFIG'].get_compose_files(env, step)
    print("***service_links", service_links)
    print("***compose_files", compose_files)

    if not compose_files and service_links:
        print("**")
        compose_files = []
        for dep in service_links:
            print("**dep", dep)
            compose_files += ctx.obj['CONFIG'].get_compose_files(ctx.obj['ENV'], dep)

    elif not compose_files:
        click.secho('Missing compose files for env: {} step:{}'.format(env, step), fg='yellow')
        return




    #Todo: use parameter for disabling the use of BUILDKIT
    TEST_BUILD = True
    USE_BUILDKIT = True

    if TEST_BUILD:
        print('TEST_BUILD',TEST_BUILD)
        print('USE_BUILDKIT',USE_BUILDKIT)

        if USE_BUILDKIT:
            os.environ['DOCKER_BUILDKIT'] = '1'

        cf = utils.eval_compose_files(compose_files)
        import pprint; pprint.pprint(cf.get('services', {}))
        for service_name, service_conf  in cf.get('services', {}).items():
            build_conf = service_conf.get('build', None)
            if not build_conf:
                click.secho('Missing build configuration for service:{}'.format(service_name), fg='yellow')
                res = 0
                continue

            context = build_conf.get('context', None)
            dockerfile = build_conf.get('dockerfile', None)
            target = build_conf.get('target', None)
            compose_build_args = build_conf.get('args', None)
            image = service_conf.get('image', None)


            build_env_vars = env_manager.get_dict_envs(ctx.obj['CONFIG'].get_build_args(env, step))
            # print('build_env_vars',compose_build_args)
            if compose_build_args:
                build_env_vars.update(compose_build_args)


            build_env_vars_args = [['--build-arg', '"{}={}"'.format(k, v)] for (k, v) in build_env_vars.items()]
            build_env_vars_args = list(itertools.chain(*build_env_vars_args))


            args = ['docker build']
            args += build_env_vars_args


            progress_plain = ctx.obj.get('JENKINS_MODE')
            if False or progress_plain:
                args += ['--progress=plain']


            if dockerfile:
                if context:
                    # dockerfile = '{}/{}'.format(context,dockerfile)
                    dockerfile = os.path.join(context,dockerfile)

                args += ['-f', dockerfile ]

            if target:
                args += ['--target', target]

            if image:
                args += ['-t', image]

            if context:
                args += [context]

            # print('Build args:',args)
            res = utils.run(args)
            print('res:',res)
            if res != 0 :
                click.secho('Error during build pass for env: {} step:{}'.format(env, step), fg='red')
                ctx.abort()




    else:
        # Construction of buuil env args
        build_env_vars = env_manager.get_dict_envs(ctx.obj['CONFIG'].get_build_args(env, step))
        build_env_vars_args = [['--build-arg', '{}={}'.format(k, v)] for (k, v) in build_env_vars.items()]
        build_env_vars_args = list(itertools.chain(*build_env_vars_args))


        compose_file_args = utils.compose_files_to_docker_command_args(compose_files)

        # Crate command
        args = ['docker compose']
        args += compose_file_args
        args += ['build']
        args += build_env_vars_args

        res = utils.run(args)

        if res != 0:
            click.secho('Build failed for service {}'.format(service_name), fg='red')
            ctx.exit()

    return res


@click.pass_context
def build(ctx, env, service, push_after_build=True):
    # print('build', env, service, push_after_build)

    for dep in ctx.obj['CONFIG'].dependency_of(service):
        """recurse build pass for any dependency"""
        ctx.invoke(build, env, dep,push_after_build)
        # ctx.invoke(push_step, env, dep)

    ctx.invoke(build_step, env, service)
    if push_after_build:
        ctx.invoke(push_step, env, service)


@click.pass_context
def push(ctx, env, service):
    print('push', env, service, ctx.obj['CONFIG'].dependency_of(service))

    if ctx.obj.get('JENKINS_MODE'):
        ctx.invoke(push_step, env, None)
    else:
        for dep in ctx.obj['CONFIG'].dependency_of(service):
            """recurse build pass for any dependency"""
            ctx.invoke(push_step, env, dep)
        ctx.invoke(push_step, env, service)


