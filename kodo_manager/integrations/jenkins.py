from jenkins import Jenkins as OrigJenkins, NotFoundException
import pprint
import requests
import json

# BUILD_INFO = '%(folder_url)sjob/%(short_name)s/%(number)d/api/json?depth=%(depth)s'
# BUILD_CONSOLE_OUTPUT = '%(folder_url)sjob/%(short_name)s/%(number)d/consoleText'

ADD_JOB_CREDENTIAL = '%(folder_url)sjob/%(short_name)s/credentials/store/folder/domain/_/createCredentials'
DELETE_JOB_CREDENTIAL = '%(folder_url)sjob/%(short_name)s/credentials/store/folder/domain/_/credential/%(identity)s/doDelete'

'http://ec2-3-224-157-64.compute-1.amazonaws.com/job/td_backend/credentials/store/folder/domain/_/credential/stage-env/doDelete'


# ADD_CREDENTIAL = '%(folder_url)sjob/%(short_name)s/%(number)d/consoleText'

# 'http://ec2-3-224-157-64.compute-1.amazonaws.com/job/td_backend/credentials/store/folder/domain/_/createCredentials'


class Jenkins(OrigJenkins):

    def __init__(self, *args, **kwargs):

        super(Jenkins, self).__init__(*args, **kwargs)
        # self.job_name = job_name

        # user = self.server.get_whoami()
        # version = self.server.get_version()
        # info = self.server.get_info()
        # jobs = self.server.get_jobs()
        #
        # print('Hello %s from Jenkins %s' % (user['fullName'], version))
        # pprint.pprint(version)
        # print('-----')
        # pprint.pprint(user)
        # pprint.pprint(info)
        # print('-----')
        # pprint.pprint(jobs)

    def delete_credential(self, identity, job_name=None):
        if job_name:
            folder_url, short_name = self._get_job_folder(job_name)
        else:
            raise NotImplementedError('Add credential only to job')

        try:
            self.jenkins_request(requests.Request(
                'POST', self._build_url(DELETE_JOB_CREDENTIAL, locals())))
        except NotFoundException:
            pass

    def set_secret_text(self, identity, secret, description=None, job_name=None):
        """Uses the jenkins API (and cURL) to set a text-based secret.
        :param identity: The ID to use to refer to the secret
        :param secret: The secret text
        :param description: The secret's description
        """
        if job_name:
            folder_url, short_name = self._get_job_folder(job_name)
        else:
            raise NotImplementedError('Add credential only to job')

        self.delete_credential(identity, job_name=job_name)

        description = description or 'Secret Text'
        description = f'({identity}) {description}'

        payload = {
            "": "0",
            "credentials": {
                "secret": secret,
                "$redact": "secret",
                "id": identity,
                "description": description.replace("'", ""),
                "stapler-class": "org.jenkinsci.plugins.plaincredentials.impl.StringCredentialsImpl",
                "$class": "org.jenkinsci.plugins.plaincredentials.impl.StringCredentialsImpl"
            },
        }
        data = {
            'json': json.dumps(payload)
        }

        resp = self.jenkins_request(requests.Request(
            'POST', self._build_url(ADD_JOB_CREDENTIAL, locals()), data=data))
        # print(type(resp))
        # print(type(resp))
        # pprint.pprint(resp.__dict__)

    def set_secret_file(self, identity, file, description=None, job_name=None):
        """Uses the jenkins API (and cURL) to set a file-based secret.
        :param identity: The ID to use to refer to the secret
        :param filename: The file to load
        :param description: The secret's description
        """
        if job_name:
            folder_url, short_name = self._get_job_folder(job_name)
        else:
            raise NotImplementedError('Add credential only to job')

        self.delete_credential(identity, job_name=job_name)

        description = description or 'Secret File'
        description = f'({identity}) {description}'

        # print('file:',file,file.name)

        files = {
            'secret': file
        }
        payload = {
            '': '4',
            'credentials': {
                'scope': 'GLOBAL',
                'id': identity,
                'file': 'secret',
                'description': description.replace("'", ""),
                '$class': 'org.jenkinsci.plugins.plaincredentials.impl.FileCredentialsImpl'
            }
        }
        data = {
            'json': json.dumps(payload)
        }

        req = requests.Request(
            'POST',
            self._build_url(ADD_JOB_CREDENTIAL, locals()),
            data=data,
            files=files
        )

        resp = self.jenkins_request(req)



    def set_secret_username_password(self, identity, username, password,
                                     description='Secret User', job_name=None):
        """Uses the jenkins API (and cURL) to set a username/password-based secret.
        :param identity: The ID to use to refer to the secret
        :param username: The user's name
        :param password: The user's password
        :param description: The secret's description
        """

    def get_secrets(self):
        url = 'http://ec2-3-224-157-64.compute-1.amazonaws.com/job/td_backend/credentials/store/folder/domain/_/api/json?pretty=true&tree=credentials[id]'
        resp = server.jenkins_open(requests.Request(
            'GET', url))

        # print(resp)
        r = json.loads(resp)
        # pprint.pprint(r)

# def test_jenkins():
#     """
#     Nel profilo utente di jenkins, creare un tocke se non gia esistente
#     serve per il login user:token
#
#
#     :return:
#     """
#     import jenkins
#     import pprint
#     import requests
#     import json
#
#     server = jenkins.Jenkins('http://ec2-3-224-157-64.compute-1.amazonaws.com', username='elfo',
#                              password='118b0fb13b5f8ef07a9843308d3feb2e89')
#     # user = server.get_whoami()
#     # version = server.get_version()
#     # info = server.get_info()
#     # jobs = server.get_jobs()
#     #
#     # print('Hello %s from Jenkins %s' % (user['fullName'], version))
#     # pprint.pprint(version)
#     # print('-----')
#     # pprint.pprint(user)
#     # print('-----')
#     # pprint.pprint(info)
#     # print('-----')
#     # pprint.pprint(jobs)
#     # print('-----')
#     # pprint.pprint(server.get_job_config('td_backend'))
#
#     url = 'http://ec2-3-224-157-64.compute-1.amazonaws.com/job/td_backend/credentials/store/folder/domain/_/api/json?pretty=true&tree=credentials[id]'
#     resp = server.jenkins_open(requests.Request(
#         'GET', url))
#
#     # print(resp)
#     r = json.loads(resp)
#     pprint.pprint(r)
#
#     # inner_params = {
#     #     "": "0",
#     #     "credentials": {
#     #         "scope": "GLOBAL",
#     #         "id": "test_id",
#     #         "username": "test user",
#     #         "password": "test password",
#     #         # "file": "file0",
#     #         # "id": "another",
#     #         # "description": "another",
#     #         "stapler-class": "org.jenkinsci.plugins.plaincredentials.impl.FileCredentialsImpl",
#     #         # "$class": "org.jenkinsci.plugins.plaincredentials.impl.FileCredentialsImpl"
#     #         # "$class": "com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl"
#     #     }
#     # }
#
#     inner_params = {"": "0",
#                     "credentials": {"username": "pippo Bella li",
#                                     "password": "pippo",
#                                     "$redact": "password",
#                                     "id": "pippo",
#                                     "description": "pippo from api",
#                                     "stapler-class": "com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl",
#                                     "$class": "com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl"},
#                     # "Jenkins-Crumb": "0bbc1f0fcd31dd860b33c24248b3f295"
#                     }
#
#     data = {
#         'json': json.dumps(inner_params)
#     }
#
#     try:
#         url = 'http://ec2-3-224-157-64.compute-1.amazonaws.com/job/td_backend/credentials/store/folder/domain/_/credential/pippo/doDelete'
#         resp = server.jenkins_open(requests.Request(
#             'POST', url))
#     except:
#         pass
#
#     url = 'http://ec2-3-224-157-64.compute-1.amazonaws.com/job/td_backend/credentials/store/folder/domain/_/createCredentials'
#     resp = server.jenkins_open(requests.Request(
#         'POST', url, data=data))
#
#     inner_params = {"": "0",
#                     "credentials": {
#                         "secret": "FOOO=PEPPE\nBAR=BAZ\nDEBUG=True",
#                         "$redact": "secret",
#                         "id": "stage-env",
#                         "description": "",
#                         "stapler-class": "org.jenkinsci.plugins.plaincredentials.impl.StringCredentialsImpl",
#                         "$class": "org.jenkinsci.plugins.plaincredentials.impl.StringCredentialsImpl"
#                     },
#                     "Jenkins-Crumb": "0bbc1f0fcd31dd860b33c24248b3f295"
#                     }
#
#     data = {
#         'json': json.dumps(inner_params)
#     }
#
#     try:
#         url = 'http://ec2-3-224-157-64.compute-1.amazonaws.com/job/td_backend/credentials/store/folder/domain/_/credential/stage-env/doDelete'
#         resp = server.jenkins_open(requests.Request(
#             'POST', url))
#     except:
#         pass
#
#     url = 'http://ec2-3-224-157-64.compute-1.amazonaws.com/job/td_backend/credentials/store/folder/domain/_/createCredentials'
#     resp = server.jenkins_open(requests.Request(
#         'POST', url, data=data))
