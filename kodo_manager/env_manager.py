import click
import logging
import os
import six
import pprint

from collections import OrderedDict

from dotenv.main import DotEnv as OrigDotEnv
# from dotenv.main import __posix_variable
from kodo_manager.utils import get_git_version, get_git_branch
from contextlib import contextmanager
import tempfile
import re

logger = logging.getLogger(__name__)

__posix_variable = re.compile(
    r"""
    \$\{
        (?P<name>[^\}:]*)
        (?::-
            (?P<default>[^\}]*)
        )?
    \}
    """,
    re.VERBOSE,
)  # type: Pattern[Text]


def resolve_nested_variables(values, autoresolve=True):
    def _replacement(name):
        """
        get appropriate value for a variable name.
        first search in environ, if not found,
        then look into the dotenv variables
        """
        ret = values.get(name, os.getenv(name))

        # Se provo a risolvere la variabile con una variabile dello stessso nome la prendo forzatamente dall'envioronment'
        if ret == '${{{}}}'.format(name):
            ret = os.getenv(name)

        if ret is None:
            click.secho('Warning: ${{{}}} not resolved'.format(name), fg='yellow' )

            if not autoresolve:
                ret = f'${{{name}}}'

        return ret

    def _re_sub_callback(match_object):
        """
        From a match object gets the variable name and returns
        the correct replacement
        """

        return _replacement(match_object.group()[2:-1])

    for k, v in values.items():
        values[k] = __posix_variable.sub(_re_sub_callback, v)

    return values


class DotEnv(OrigDotEnv):
    def __init__(self, *args, **kwargs):
        self.autoresolve = kwargs.pop('autoresolve')
        super(DotEnv, self).__init__(*args, **kwargs)

    def dict(self):
        """Return dotenv as dict"""
        if self._dict:
            return self._dict

        values = OrderedDict(self.parse())
        self._dict = resolve_nested_variables(values, autoresolve=self.autoresolve)
        return self._dict



def dotenv_values(dotenv_path=None, stream=None, verbose=False, autoresolve=True):
    f = dotenv_path or stream or find_dotenv()
    return DotEnv(f, verbose=verbose, autoresolve=autoresolve).dict()


@contextmanager
def custom_environment(envs):
    """
    Save initial env.
    load new env on enter

    restore previus env on exit

    :param envs:
    :return:
    """
    initial_env = os.environ.copy()

    try:
        # os.environ.clear()
        for k, v in envs.items():
            os.environ[k] = v
        yield os.environ
    finally:
        os.environ.clear()
        os.environ.update(initial_env)


@contextmanager
def cleaned_environment():
    """
    Clen from previus env

    :param envs:
    :return:
    """
    initial_env = os.environ.copy()

    try:
        os.environ.clear()
        if 'HOME' in initial_env:
            os.environ['HOME'] = initial_env.get('HOME')
        yield os.environ
    finally:
        os.environ.update(initial_env)


@click.pass_context
def assert_environ(ctx, environ, env, step):
    required_vars = ctx.obj['CONFIG'].get_env_vars(env, step)
    for k in required_vars:
        if k not in os.environ:
            raise Exception('Var {} not in Env'.format(k))


@click.pass_context
def make_default_env_dict(ctx, env=None):
    if ctx.obj['JENKINS_SETUP']:
        print('In jenkins setup, skip __default__ envs')

        return {}

    # print('make_default_env_dict',ctx.__dict__)
    # pprint.pprint(ctx.__dict__)
    env_vars = {
         '__KM_ENVIRONMENT__': env,
    }
    defaults_map = ctx.obj['CONFIG'].get_env_defaults()
    # pprint.pprint(defaults_map)
    for k in defaults_map:

            default_var = defaults_map[k]
            if isinstance(default_var, tuple):
                action, args = default_var
                if action == '__git_tag__':
                    env_vars[k] = get_git_version(args)
                elif    action == '__git_branch__':
                    env_vars[k] = get_git_branch(args)
                elif action == 'os.path.expanduser':
                    env_vars[k] = os.path.expanduser(args)
                else:
                    raise Exception('Action not know')
            elif isinstance(default_var, str):
                env_vars[k] = default_var
            else:
                raise Exception('Env default typ unknow')
    return env_vars


@click.pass_context
def get_env(ctx, env, step):
    if ctx.obj.get('JENKINS_MODE'):
        print('Todo: setup jenkins  get_env da env ')

        environ = OrderedDict({})
        for k,v in ctx.obj['CONFIG'].get_default_env().items():
            # print('@@@@ def env',k,v)
            if isinstance(v, (tuple,list)):
                action, args = v
                if action == '__git_tag__':
                    # In jenkis use current dir for git_tag
                    try:
                        environ[k] = get_git_version('.')
                    except:
                        environ[k] = "${GIT_TAG}"
                elif action == 'os.path.expanduser':
                    pass
                    # environ[k] = os.path.expanduser(args)
                else:
                    raise Exception('Action not know')

        # print(environ)
        environ.update(ctx.obj['CONFIG'].get_jenkins_environ())
        environ.update(ctx.obj['ENV_FILES'])

        # environvalues = OrderedDict(environ)

        # print(environ)
        return resolve_nested_variables(environ, autoresolve=True)

    # defaults_map = ctx.obj['CONFIG'].get_env_defaults()
    defaults_map = make_default_env_dict(env=env)
    # os.environ.update(defaults_map)


    env_vars = {
        '__KM_ENVIRONMENT__': env,
        '__KM_DOCKER_GROUP_NAME__': ctx.obj['CONFIG'].get_docker_group_name(env),
    }



    # Load from env files
    env_files = ctx.obj['CONFIG'].get_setup_env_files(env, step)
    # print('env_files',env_files)

    with cleaned_environment() as environ:
        os.environ.update(defaults_map)
        for file, insert_in_file in get_env_sources_files(env_files,env):
            if ctx.obj['JENKINS_SETUP']:
                dotenv_autoresolve = False
            else:
                dotenv_autoresolve = True

            envs = dotenv_values(verbose=True, dotenv_path=file, autoresolve=dotenv_autoresolve)
            # print('envs',envs)
            env_vars.update(envs)
            # print('env_vars',env_vars)
            environ.update(env_vars)

    # Add variable of endfile
    try:
        env_vars.update(ctx.obj['ENV_FILES'])
    except:
        pass

    # print('>>>env_vars', env_vars)
    for k in defaults_map:
        if k not in env_vars:
            env_vars[k] = defaults_map[k]
            # default_var = defaults_map[k]
            # if isinstance(default_var, tuple):
            #     action, args = default_var
            #     if action == '__git_tag__':
            #         env_vars[k] = get_git_version(args)
            #     else:
            #         raise Exception('Action not know')
            # elif isinstance(default_var, str):
            #     env_vars[k] = default_var
            # else:
            #     raise Exception('Env default typ unknow')
    # print('___env_vars', env_vars)
    return env_vars


@click.pass_context
def get_env_sources_files(ctx, import_list, env):
    sources_list = []

    base_path = ctx.obj['CONFIG'].base_path

    for i in import_list:

        if isinstance(i, tuple):
            path, insert_in_file = i
        elif isinstance(i, six.string_types):
            path = i
            insert_in_file = True
        else:
            raise NotImplementedError()

        if not os.path.exists(path):
            path = os.path.join(base_path, path)

        if not os.path.exists(path):
            click.secho('{} not found'.format(path), fg='red')
            ctx.exit()

        if os.path.isfile(path):
            sources_list.append((path, insert_in_file))
        elif os.path.isdir(path):
            resolved = False

            source_files = ['default.env', '{}.env'.format(env) ]
            if ctx.obj['JENKINS_SETUP']:
                source_files.append('jenkins.env')

            for fname in source_files:
                file_path = os.path.join(path, fname)
                if os.path.exists(file_path) and os.path.isfile(file_path):
                    sources_list.append((file_path, insert_in_file))
                    resolved = True
            if not resolved:
                click.secho('Missing env file in {} dir'.format(path), fg='red')
                ctx.exit()



    return sources_list


@contextmanager
@click.pass_context
def with_env_files(ctx, env):
    '''
    Questa funzione viene usata con with statment.
    Un generatore che crea dei file temporanei su disco e li rimuove al termine


    :param ctx:
    :param env:
    :return:
    '''
    if ctx.obj.get('JENKINS_MODE'):



        try:
            print('Todo: setup jenkins  with_env_files da env ')
            teporary_env_file_map = {}
            opened_files = []
            for varname,content in ctx.obj['CONFIG'].get_env_files(env).items():
                # print(varname,content)
                fp = tempfile.NamedTemporaryFile(mode='w+t', suffix='.env', delete=False)
                fp.write(content)
                fp.flush()
                opened_files.append(fp)
                teporary_env_file_map[varname] = fp.name
            ctx.obj['ENV_FILES'] = teporary_env_file_map

            # print(teporary_env_file_map)
            yield
        finally:
            for f in opened_files:
                f.close()
            ctx.obj['ENV_FILES'] = None
            # print('clean')


    else:
        try:
            env_files = ctx.obj['CONFIG'].get_env_files(env)

            teporary_env_file_map = {}
            opened_files = []
            with cleaned_environment():
                os.environ.update(make_default_env_dict(env=env))

                for name, import_list in env_files.items():
                    varname = 'ENVFILE_{}'.format(name)
                    env_vars = {}

                    for file, insert_in_file in get_env_sources_files(import_list, env):
                        if ctx.obj['JENKINS_SETUP']:
                            dotenv_autoresolve = False
                        else:
                            dotenv_autoresolve = True

                        envs = dotenv_values(verbose=True, dotenv_path=file, autoresolve=dotenv_autoresolve)
                        
                        os.environ.update(envs)
                        if insert_in_file:
                            env_vars.update(envs)

                    fp = tempfile.NamedTemporaryFile(mode='w+t', suffix='.env', delete=False)
                    for k, v in env_vars.items():
                        if " " in v:
                            click.secho(f'ERROR: key: "{k}"" contain space. protect value "{v}" ', fg='yellow' )
                            v = f"'{v}'"
                            # raise Exception(f'ERROR: key: "{k}"" contain space. protect value "{v}" ')
                        line_out = '{}={}'.format(k, v)

                        fp.write("{}\n".format(line_out))
                    fp.flush()
                    opened_files.append(fp)
                    teporary_env_file_map[varname] = fp.name
            ctx.obj['ENV_FILES'] = teporary_env_file_map
            print(teporary_env_file_map)
            yield
        finally:
            for f in opened_files:
                f.close()
            ctx.obj['ENV_FILES'] = None


@contextmanager
@click.pass_context
def load_env(ctx, env, step):
    # print('Load env for env:{} step:{}'.format(env, step))

    env_vars = get_env(env, step)
    # print('load_env env_vars',env_vars)
    try:
        with custom_environment(env_vars) as environ:

            assert_environ(environ, env, step)
            yield environ
    finally:
        pass


def get_dict_envs(list):
    envs = {}
    for k in list:
        if k not in os.environ:
            raise Exception('Var {} not in Env'.format(k))

        envs[k] = os.environ[k]
    return envs


@click.pass_context
def environ(ctx):
    return load_env(
        ctx.obj['ENV'],
        ctx.obj['SERVICE']
    )
