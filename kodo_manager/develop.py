import click
from kodo_manager import utils, env_manager
from kodo_manager.click_extension import ServicesOption
from kodo_manager.commands import DockerCommandGroupWrapper
from kodo_manager.build import build_step


def callback(ctx, args, incomplete):
    print('callback', ctx, args, incomplete)
    # Completions returned as strings do not have a description displayed.
    return incomplete


@click.group(cls=DockerCommandGroupWrapper)
@click.option('--service', '-s', cls=ServicesOption, required=True)
@click.pass_context
def develop(ctx, service):
    # print('develop', service)
    ctx.obj['SERVICE'] = ''.join(service)
    ctx.obj['ENV'] = ctx.parent.invoked_subcommand
    pass


@click.pass_context
def load_compose_args(ctx):
    docker_group_name = ctx.obj['CONFIG'].get_docker_group_name(ctx.obj['ENV'])
    print('docker_group_name',docker_group_name)
    print("ctx.obj['ENV']",ctx.obj['ENV'])

    service_links = ctx.obj['CONFIG'].get_linked_services(ctx.obj['SERVICE']) or [ctx.obj['SERVICE']]
    print("service_links",service_links)
    vargs = ['docker compose']
    for dep in service_links:
        compose_files = ctx.obj['CONFIG'].get_compose_files(ctx.obj['ENV'], dep)
        compose_args = utils.compose_files_to_docker_command_args(compose_files)
        vargs += compose_args

    vargs += ['-p', docker_group_name]
    return vargs


@develop.command(name='run',context_settings={"ignore_unknown_options": True})
# @click.option('--command', '-c', default=None, type=str)
@click.option('--docker-service', '-ds', type=str)
@click.argument('command', nargs=-1)
@click.pass_context
def run(ctx,  docker_service, command):
    click.secho(f'####### Run command {command} --> service:{docker_service}##########', fg='green')
    # print('run command', docker_service, command)
    if not docker_service:
        docker_service = ctx.obj['SERVICE']


    if isinstance(command, str):
        command = command.split()

    command = list(command)


    with env_manager.environ():
        vargs = load_compose_args()
        vargs += ['run', '--rm', docker_service]
        vargs += command

        utils.run(vargs)


@develop.command(name='exec')
@click.option('--command', '-c', default=None, type=str)
@click.option('--docker-service', '-ds', type=str)
@click.pass_context
def exec(ctx, command, docker_service):
    if not docker_service:
        docker_service = ctx.obj['SERVICE']
    with env_manager.environ():
        vargs = load_compose_args()
        vargs += ['exec', docker_service]
        vargs += command.split()

        utils.run(vargs)


@develop.command(name='config')
@click.pass_context
def config(ctx,):
    with env_manager.environ():
        vargs = load_compose_args()
        vargs += ['config']

        utils.run(vargs)
        
        # ctx.invoke(logs)


@develop.command(name='env')
@click.option('--service', '-s', cls=ServicesOption, required=True)
@click.pass_context
def env(ctx, service):
    env = ctx.obj['ENV']

    environ = env_manager.get_env(env, service)
    # print('environ',environ)
    for k,v in environ.items():
        print('export {}="{}"'.format(k,v))


@develop.command(name='up',aliases=['start'])
@click.option('--nobuild',type=bool, default=False, flag_value=True)
# @click.argument('extra_args', nargs=-1)
@click.pass_context
def up(ctx, nobuild):
    with env_manager.environ():
        vargs = load_compose_args()
        vargs += ['up', '-d']

        if not nobuild:
            env = ctx.obj['ENV']
            service = ctx.obj['SERVICE']
            print('Require build',env,service)



            ctx.invoke(build_step, env, service)

        print('Vargs:',vargs)

        utils.run(vargs)
        ctx.invoke(logs)


@develop.command(name='stop')
@click.pass_context
def stop(ctx):
    with env_manager.environ():
        vargs = load_compose_args()
        vargs += ['stop']
        utils.run(vargs)

        vargs = load_compose_args()
        vargs += ['rm', '-vf']
        utils.run(vargs)

@develop.command(name='restart')
@click.pass_context
def restart(ctx):
    ctx.invoke(stop)
    ctx.invoke(up)


@develop.command(name='logs')
@click.pass_context
def logs(ctx):
    with env_manager.environ():
        vargs = load_compose_args()
        vargs += ['logs', '-f', '--tail', '200']
        utils.run(vargs)



    # todo:
    # comando per caricare le variabili nella shell, che salva lo stato precedente e comando per resettare le variabili caricate