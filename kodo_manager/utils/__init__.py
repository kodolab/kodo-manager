import subprocess
import click
import sys
import itertools
import os
import botocore.session 
import base64

# from yaml import load, CLoader as Loader
from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


def get_git_branch(path):
    """
    Restituisce il codice dell'ultimo commit di una directory git (del branch attivo)

    :param path:
    :return:
    """
    # todo: ottenere i codice in funzione del branch
    # todo: se il commit del repo ha un tag, restituire il valore del tag
    # cmd = 'git  -C {} describe --abbrev=4 --tags'.format(path)
    # try:
    #     return subprocess.check_output(cmd, shell=True).strip().decode("utf-8")
    # except:
    #     raise Exception('uneble to detect git version of path {}'.format(path))

   
    cmd = f'git -C {path} rev-parse --abbrev-ref --short HEAD'
    try:
        return subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT).strip().decode("utf-8")
    except:
        return '__undefined__'
        # raise Exception('uneble to detect git version of path {}'.format(path))

def get_git_version(path):
    """
    Restituisce il codice dell'ultimo commit di una directory git (del branch attivo)

    :param path:
    :return:
    """
    # todo: ottenere i codice in funzione del branch
    # todo: se il commit del repo ha un tag, restituire il valore del tag
    # cmd = 'git  -C {} describe --abbrev=4 --tags'.format(path)
    # try:
    #     return subprocess.check_output(cmd, shell=True).strip().decode("utf-8")
    # except:
    #     raise Exception('uneble to detect git version of path {}'.format(path))

    cmd = f'git -C {path} describe  --exact-match --tags'
    try:
        return subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT).strip().decode("utf-8")
    except:
        pass
        # raise Exception('uneble to detect git version of path {}'.format(path))


    cmd = f'git -C {path} rev-parse --short HEAD'
    try:

        return subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT).strip().decode("utf-8")
    except:
        raise Exception('uneble to detect git version of path {}'.format(path))


def compose_files_to_docker_command_args(compose_files, opt_name='-f'):
    args = [[opt_name, f] for f in compose_files]
    args = list(itertools.chain(*args))
    return args


@click.pass_context
def run(ctx, cmd, stdout_parser_func=lambda x: x):
    """
    Esegue il comand "cmd" con subprocess.
    Logga su file (da definire)
    logga in console se in modalitá debug

    parsa ogni line con la funzione stdout_parser_func e ritorna il risultato


    :param ctx:
    :param cmd:
    :param stdout_parser_func:
    :return:
    """

    if isinstance(cmd, list):
        cmd_list = cmd
    else:
        cmd_list = cmd.split()

    click.echo('Run cmd: ', nl=False)
    click.secho(' '.join(cmd_list), fg='bright_cyan', underline=True, blink=True)
    return os.system(' '.join(cmd_list))

    #
    # with subprocess.Popen(cmd_list, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, bufsize=1) as p, \
    #         open('/tmp/foo', 'ab') as file:
    #     res = None
    #     for line in p.stdout:  # b'\n'-separated lines
    #         if ctx.obj.get('DEBUG', False):
    #             sys.stdout.buffer.write(line)  # pass bytes as is
    #             sys.stdout.flush()
    #         sys.stdout.buffer.write(line)  # pass bytes as is
    #         sys.stdout.flush()
    #         file.write(line)
    #
    #         res = stdout_parser_func(line.decode("utf-8"))
    #     p.wait()
    #
    #
    #
    # return res

@click.pass_context
def eval_compose_files(ctx, files, clean_stdout=False):
    cmd_list = ['docker compose']
    cmd_list += compose_files_to_docker_command_args(files, opt_name='-f')
    cmd_list += ['config']
    cmd = ' '.join(cmd_list)
    result = subprocess.run(cmd, stdout=subprocess.PIPE, shell=True)
    # print('result:',result.stdout)
    if clean_stdout:
        return result.stdout.decode("utf-8") 
    data = load(result.stdout, Loader=Loader)
    if not data:
        click.secho('Failed evaluation of compose file {}'.format(files), fg='red')
        click.secho('Fail command: {}'.format(' '.join(cmd_list)), fg='red')
        ctx.exit()

    return data

@click.pass_context
def docker_login(ctx):
    registry_login = ctx.obj['CONFIG'].get_registry_login()
    if not registry_login:
        return
    
    if 'ecr' in registry_login:
        session  = botocore.session.get_session()
        client = session.create_client('ecr',**registry_login['ecr'])
        response = client.get_authorization_token()
        authorizationToken = response.get('authorizationData')[0].get('authorizationToken')
        proxyEndpoint = response.get('authorizationData')[0].get('proxyEndpoint')

        data = base64.b64decode(authorizationToken)
        [username,password] = data.decode().split(':')

        args = ['docker', 'login' , '--username', username, '--password',password, proxyEndpoint ]

        res = run(args)
        if res != 0:
            click.secho('Error during docker_login to proxyEndpoint: {} '.format(proxyEndpoint), fg='red')
            ctx.abort()
        return
    
    raise NotImplementedError
