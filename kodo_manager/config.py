import os
import logging
import json
import pprint
from kodo_manager import utils, env_manager
from kodo_manager.notifications import get_notifier
from kodo_manager.integrations.jenkins import Jenkins
# from kodo_manager.crypto import CryptoManager

logger = logging.getLogger(__name__)


class ConfigManager():
    def __init__(self, config, base_path=None):
        self._config = config
        self.base_path = base_path
        # self.crypto = CryptoManager(config=self)


    def envs(self):
        """ return list of step defined in conf"""
        return [k for k in self._config.get('envs', {}).keys() if not k.startswith('__')]

    def steps(self):
        """ return list of step defined in conf"""
        return list(self._config.get('items', {}).keys())

    def get_docker_machine(self, env):
        return self._config.get('envs', {}).get(env, {}).get('docker_machine', None)

    def get_step_conf(self, step):
        return self._config.get('items', {}).get(step)

    def get_step_workdir(self, step):
        return self._config.get('items', {}).get(step, {}).get('work_dir', None)

    def get_docker_group_name(self, env):
        return self._config.get('envs', {}).get(env, {}).get('docker_group_name', {}) or self._config.get('docker_group_name', 'kodo_manager_group')

    def get_registry_login(self):
        return self._config.get('registry_login', None)

    def get_compose_files(self, env, step, absolute_path=True):
        step_conf = self.get_step_conf(step)
        files = step_conf.get('build_compose_files',{})

        files = step_conf.get('envs', {}).get(env,{}).get('build_compose_files', None) or files


        workdir = self.get_step_workdir(step)
        # print('get_compose_files',env,step,workdir)

        # check if compose-file exists. search in local folder or in step-workdir folders
        compose_files = []
        for f in files:
            # print('search file:',f, 'wd:',workdir)
            if workdir:
                wf = os.path.join(workdir, f)
                wf = os.path.expanduser(wf)
                if os.path.exists(wf):

                    if not absolute_path:
                        compose_files.append(f)
                    else:
                        compose_files.append(wf)
                    continue

            if os.path.exists(f):
                compose_files.append(f)
                continue
            raise Exception('Compose file {} not found env:[{}] step:[{}] workdir:[{}]'.format(f,env,step,workdir))
        # print('##compose_files',compose_files)
        return compose_files

    def dependency_of(self,step):

        deps = self._config.get('items', {}).get(step, {}).get('dependency', [])

        if isinstance(deps, str):
            deps = [deps]

        return deps

    def get_linked_services(self, step):
        return self._config.get('items', {}).get(step, {}).get('linked_service', [])

    def get_env_files(self, env, step=None):
        # print('get_env_files',env,step)
        # print(self._config.get('envs', {}).get(env, {}).get('env_files', {}) or self._config.get('envs', {}).get('__default__', {}).get('env_files', {}))
        return self._config.get('envs', {}).get(env, {}).get('env_files', {}) or self._config.get('envs', {}).get('__default__', {}).get('env_files', {})

    def get_setup_env_files(self, env, step=None):
        # print('get_setup_env_files',env,step)
        return self._config.get('envs', {}).get(env, {}).get('setup_env_files', []) or self._config.get('envs', {}).get('__default__', {}).get('setup_env_files', [])

    def get_build_args(self, env,step):
        return self._config.get('items', {}).get(step, {}).get('build_args', [])

    def get_env_vars(self, env, step):
        return self._config.get('items', {}).get(step, {}).get('env_vars', [])

    def get_env_defaults(self):
        return dict([(k,v) for (k,v) in self._config.get('envs', {}).get('__default__', {}).items() if k not in ['setup_env_files', 'env_files']])

    def get_docker_command_keys(self):
        return self._config.get('docker_commands', {}).keys()

    def get_docker_command_spec(self, command_name):
        return self._config.get('docker_commands', {}).get(command_name,None)

    def is_protected_env(self, env):
        return self._config.get('envs', {}).get(env, {}).get('protected', False)

    def get_notifier(self, env):
        slack = self._config.get('envs', {}).get(env, {}).get('slack', False)
        if slack:
            return get_notifier('slack', env, url=slack)

        return get_notifier(None, env)

    def get_jenkins_conf(self, env, step):
        conf = self._config.get('jenkins', {})
        conf.update(self._config.get('items', {}).get(step,{}).get('jenkins',{}))

        return conf


class JenkinsConfigManager(ConfigManager):
    """
    This class get configuration readed by env
    """

    def __init__(self, env, base_path=None):
        print('JenkinsConfigManager init',env)
        try:
            json_data = os.environ['JANKINS_KODOMANAGER_ENV']
        except KeyError:
            msg = '''
Kodo manager to work in jenkins must read the 
configuration from the environment variables.

configures jenkins from your laptop:

km -c conf <env> jenkins

Setup your jenkinsfile to read the conf and put in JANKINS_KODOMANAGER_ENV:

withCredentials([string(credentialsId: '{env}-{service}', variable: 'JANKINS_KODOMANAGER_ENV')]) {
    sh 'echo $JANKINS_KODOMANAGER_ENV'
}

            '''
            raise Exception(f'Bad configuration\n{msg}')

        self._config = json.loads(json_data)

        # pprint.pprint(self._config)

    def get_default_env(self):
        return self._config['default_env']

    def get_compose_files(self,env,step):
        return self._config['compose_files']

    def get_env_files(self,*args):
        return self._config['environ_files']

    def get_jenkins_environ(self):
        return self._config['environ']

    def get_docker_machine(self, env):
        return self._config['docker_machine_conf']