
import os
import fs
from fs import open_fs
import gnupg



class CryptoManager():

    def __init__(self, config):
        self.config = config
        self.init()
        self.gpg = gnupg.GPG(gnupghome=self.base_crypto_path)
        self.gpg = gnupg.GPG()

        print(' public_keys', self.gpg.list_keys())
        print(' private_keys', self.gpg.list_keys(True))

        # encrypted_data = self.gpg.encrypt('foo', ['paolo@digitalmonkeys.it'])
        # print('encrypted_data',encrypted_data,encrypted_data.data,encrypted_data.__dict__)


    @property
    def base_crypto_path(self):
        return '{}/.crypto_space'.format(self.config.base_path)

    def init(self):
        if os.path.exists(self.base_crypto_path):
            print("Encrypt directory exists")
        else:
            os.makedirs(self.base_crypto_path)
            print("Created encrypted directory")



