import os
import six
import subprocess
import sys
import re
from kodo_manager.env_manager import custom_environment
import docker
from contextlib import contextmanager
import pprint
import click
from tempfile import NamedTemporaryFile, TemporaryDirectory
import tarfile
# from io import BufferedRWPair, BufferedReader, DEFAULT_BUFFER_SIZE

DOCKER_KEYS = [
    'DOCKER_TLS_VERIFY',
    'DOCKER_HOST',
    'DOCKER_CERT_PATH',
    'DOCKER_MACHINE_NAME'
]

from io import BytesIO
import time

def copy_to_container(cli, container_id, src, dst):
    with create_archive(src, dst) as archive:
        container = cli.containers.get(container_id)
        try:
            res = container.put_archive( path='/', data=archive)
        except Exception as e:
            print(e)

def create_archive(artifact_file, dst):
    pw_tarstream = BytesIO()
    pw_tar = tarfile.TarFile(fileobj=pw_tarstream, mode='w')
    file_data = open(artifact_file, 'rb').read()
    tarinfo = tarfile.TarInfo(name=dst)
    tarinfo.size = len(file_data)
    tarinfo.mtime = time.time()
    # tarinfo.mode = 0600
    pw_tar.addfile(tarinfo, BytesIO(file_data))
    pw_tar.close()
    pw_tarstream.seek(0)
    return pw_tarstream




class DockerManager:
    def __init__(self):
        """clear docker env"""
        self.clean()

        self.machine_env_map = {}

    @contextmanager
    def with_docker_machine(self, conf):

        if isinstance(conf, six.string_types):
            # When conf is a string: expected name of docker-machine
            vars = self.get_machine_env(conf)
        elif isinstance(conf, dict):
            vars = conf.copy()

        if not 'DOCKER_HOST' in vars.keys():
            click.secho('Warning: Cehck configuration of docker machine. DOCKER_HOST is not setted ', fg='yellow')
            click.get_current_context().exit()


        if not vars.get('DOCKER_CERT_PATH'):
            print('probably is an exported machine')
            dir = TemporaryDirectory(suffix='_certs')

            for var_name, fname in [
                ('TLSCACERT', 'ca.pem'),
                ('TLSCERT', 'cert.pem'),
                ('TLSKEY', 'key.pem')
            ]:
                fpath = os.path.join(dir.name,fname)
                with  open(fpath, mode='w+t') as f:
                    f.write(vars[var_name])
                    del vars[var_name]
                    f.close()

            vars['DOCKER_CERT_PATH'] = dir.name

        with custom_environment(vars) as environ:
            click.secho(f'####### Docker [{vars}] ##########', fg='green')
            dirname= vars['DOCKER_CERT_PATH']
            print(f'ls in {dirname}:', os.listdir(dirname))
            try:
                client = docker.from_env()
                # print('docker client info:')
                # pprint.pprint(client.info())
                # print('LOGIN:', login_response)
                yield client
            finally:
                pass

    def get_machine_env(self, machine):
        if machine in self.machine_env_map:
            return self.machine_env_map.get(machine)


        cmd = ['docker-machine', 'env', machine]
        env = {}

        with subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, bufsize=1) as p:

            for line in p.stdout:  # b'\n'-separated lines
                m = re.match('(export )(\w+)(=)"(.*)"',line.decode("utf-8"))
                if m:
                    _, key, _, value = m.groups()
                    env[key] = value
            p.wait()

        if not env:
            click.secho('Docker machine [{}] Not found!'.format(machine), fg='bright_red')
            click.get_current_context().exit()


        self.machine_env_map[machine] = env
        return env

    def clean(self):
        for key in DOCKER_KEYS:

            try:
                del os.environ[key]
            except:
                pass

    def export(self, machine):
        env = self.get_machine_env(machine).copy()
        print(env)

        docker_cert_path = env['DOCKER_CERT_PATH']

        tlscacert = os.path.join(docker_cert_path,'ca.pem')
        tlscert = os.path.join(docker_cert_path,'cert.pem')
        tlskey = os.path.join(docker_cert_path,'key.pem')

        env['DOCKER_CERT_PATH'] = None


        for var_name, fname in [
            ('TLSCACERT',tlscacert),
            ('TLSCERT',tlscert),
            ('TLSKEY',tlskey)
        ]:
            with open(fname) as file:
                # print(file.read())
                env[var_name] =file.read()

        return env
