from .base import BaseNotificationHelper
from .slack import NotificationHelper as SlackNotificationHelper


def get_notifier(type, *args, **kwargs):

    if type and type.lower() == 'slack':
        return SlackNotificationHelper(*args, **kwargs)

    return BaseNotificationHelper(*args, **kwargs)