


class BaseNotificationHelper():

    def __init__(self, env):
        self.env = env

    def send(self, message):
        print(f'BaseNotificationHelper: {message}')

    def deploy(self, env, service):
        message = f'Deploy env: [{env}] service: [{service}]'
        self.send(message)