from .base import BaseNotificationHelper

import json
import requests
import getpass


class NotificationHelper(BaseNotificationHelper):
    def __init__(self, *args, **kwargs):
        self.url = kwargs.pop('url')
        super(NotificationHelper, self).__init__(*args, **kwargs)

    def send(self, slack_data):
        data = json.dumps(slack_data)

        requests.post(
            self.url,
            data=data,
            headers={'Content-Type': 'application/json'}
        )



    def deploy(self, env, service):
        message = f'Deploy env: [{env}] service: [{service}]'
        # username = getpass.getuser()

        slack_data = {
            "text": 'deploy notification',
            "icon_emoji": ':rocket:',

            "attachments": [
                {
                    "fallback": message,
                    "color": "#2eb886",
                    # "pretext": "Optional text that appears above the attachment block",
                    "author_name": getpass.getuser(),
                    # "author_link": "http://flickr.com/bobby/",
                    # "author_icon": "http://flickr.com/icons/bobby.jpg",
                    "title": f"Deploy service '{service}'  to {env} env",
                    # "title_link": "https://api.slack.com/",
                    # "text": "Optional text that appears within the attachment",
                    # "fields": [
                    #     {
                    #         "title": "Priority",
                    #         "value": "High",
                    #         "short": False
                    #     }
                    # ],
                    # "image_url": "http://my-website.com/path/to/image.jpg",
                    # "thumb_url": "http://example.com/path/to/thumb.png",
                    "footer": "Kodo Manager Notifier - Slack API",
                    # "footer_icon": "https://platform.slack-edge.com/img/default_application_icon.png",
                    # "ts": 123456789
                }
            ]

        }

        self.send(slack_data)