import click
import os
import pprint
import re
import json
import itertools
from .build import build as buildCommand, push as pushCommand
from .deploy import deploy as deployCommand, pull as pullCommand, imageNameCommand
from dotenv.cli import list as _list
from dotenv.cli import set as _set
from dotenv.cli import unset as _unset
from kodo_manager import utils
from kodo_manager.click_extension import ServicesOption
from kodo_manager.commands import DockerCommandGroupWrapper
from kodo_manager import env_manager
from kodo_manager.integrations.jenkins import Jenkins
from kodo_manager.utils import eval_compose_files

import tempfile


def pass_service(func):
    @click.pass_context
    def inner(ctx, *args, **kwargs):
        service = ctx.obj['SERVICE']
        return func(service, *args, **kwargs)
    return inner

@click.group(cls=DockerCommandGroupWrapper)
@click.option('--service', '-s', cls=ServicesOption, required=True)
@click.pass_context
def wrapper(ctx,service):
    print('Wrapper',service)
    ctx.obj['SERVICE'] = ''.join(service)
    # ctx.exit()
    pass


@wrapper.command(name='build')
@click.option('--push/--no-push', default=True)
@pass_service
@click.pass_context
def wrapper_build(ctx, service, push):
    env = ctx.parent.parent.invoked_subcommand
    ctx.invoke(buildCommand, env, service, push_after_build=push)


@wrapper.command(name='push')
@pass_service
@click.pass_context
def wrapper_push(ctx, service):
    env = ctx.parent.parent.invoked_subcommand
    ctx.invoke(pushCommand, env, service)

@wrapper.command(name='image_name')
@pass_service
@click.pass_context
def wrapper_image_name(ctx, service):
    env = ctx.parent.parent.invoked_subcommand
    ctx.invoke(imageNameCommand, env, service)

@wrapper.command(name='pull')
@pass_service
@click.pass_context
def wrapper_pull(ctx, service):
    env = ctx.parent.parent.invoked_subcommand
    ctx.invoke(pullCommand, env, service)


@wrapper.command(name='deploy')
@click.option('--update', type=bool, default=False, flag_value=True)
@click.option('--build', type=bool, default=False, flag_value=True)
@click.option('--nopull', type=bool, default=False, flag_value=True)
@pass_service
@click.pass_context
def wrapper_deploy(ctx, service, update, build, nopull):
    env = ctx.parent.parent.invoked_subcommand
    if build:
        nopull = False
        ctx.invoke(buildCommand, env, service)
    ctx.invoke(deployCommand, env, service, update, nopull)


@wrapper.command(name='docker',aliases=['d'])
@click.argument('extra_args', nargs=-1)
@pass_service
@click.pass_context
def wrapper_docker(ctx, service, extra_args):
    env = ctx.parent.parent.invoked_subcommand
    docker_machine = ctx.obj['CONFIG'].get_docker_machine(env)

    with ctx.obj['DOCKER_MANAGER'].with_docker_machine(docker_machine) as docker_client:
        cmd_list = ['docker'] + list(extra_args)
        res = utils.run(cmd_list)
        if res != 0:
            click.secho('Error during docker command {} for env: {} step:{}'.format(cmd_list, env, service), fg='red')
            ctx.abort()




@click.pass_context
def print_env_files(ctx):
    print('###############################')
    print('########## ENV files ##########')
    print('###############################')
    for name, filepath in ctx.obj['ENV_FILES'].items():
        print()
        print('#### {}: {}'.format(name,filepath))
        with open(filepath,'r') as f:
            for l in f.readlines():
                print(l, end='')


@wrapper.command(name='env')
@pass_service
@click.pass_context
def wrapper_env(ctx, service):
    env = ctx.parent.parent.invoked_subcommand

    # for k,v in os.environ.items():
    for k,v in env_manager.get_env(ctx.obj['ENV'], None).items():
        print('export {}="{}"'.format(k,v))

    print_env_files()



@wrapper.command(name='env_command')
@click.argument('extra_args', nargs=-1)
@click.option('--docker', '-d', type=bool, default=False, flag_value=True)
@pass_service
@click.pass_context
def wrapper_env_command(ctx, service, docker, extra_args):
    env = ctx.parent.parent.invoked_subcommand

    if docker:
        docker_machine = ctx.obj['CONFIG'].get_docker_machine(env)
        with ctx.obj['DOCKER_MANAGER'].with_docker_machine(docker_machine) as docker_client:
            print('wrapper_env_command', env, service,docker, extra_args)
            res = utils.run(list(extra_args))
    else:
        print('wrapper_env_command', env, service, docker, extra_args)
        res = utils.run(list(extra_args))

    if res != 0:
        click.secho('Error for command: {}'.format(extra_args), fg='red')
        ctx.abort()



@wrapper.command(name='jenkins')
@click.option('--dry', '-d', type=bool, default=False, flag_value=True,  required=False)
@pass_service
@click.pass_context
def wrapper_jenkins(ctx, service,  dry):
    print('------------ Jenkins setup:',service, ' dry', dry)


    env = ctx.parent.parent.invoked_subcommand
    
 
    
    service_list = ctx.obj['CONFIG'].dependency_of(service)
    if service != 'all':
        service_list.append(service)

    for step in service_list:
        compose_files = ctx.obj['CONFIG'].get_compose_files(env, step)
        # print('compose_files',compose_files)
        
        union_compose_file = eval_compose_files(compose_files, clean_stdout=True)

        # remove context path (local)
        union_compose_file = re.sub(r'.*context:.*\n', '', union_compose_file)
        # print('union_compose_file',union_compose_file)
        
    
    
        docker_machine = ctx.obj['CONFIG'].get_docker_machine(env)
        docker_machine_conf = ctx.obj['DOCKER_MANAGER'].export(docker_machine)

        env_data = {
            'docker_machine_conf': docker_machine_conf,
            'default_env': ctx.obj['CONFIG'].get_env_defaults(),
            'environ':{

            },
            'environ_files':{

            },
            'compose_files': ctx.obj['CONFIG'].get_compose_files(env, step, absolute_path=False),
            # 'union_compose_file':union_compose_file,
            'docker_group_name': ctx.obj['CONFIG'].get_docker_group_name(env),
            'get_registry_login': ctx.obj['CONFIG'].get_registry_login()
        }

        print('Jenkins setup:', env, step)
        jenkins_conf = ctx.obj['CONFIG'].get_jenkins_conf(env, step)

        job_name = jenkins_conf.pop('job_name')
        jenkins = Jenkins(**jenkins_conf)

        # jenkins.set_secret_text('secret-file-id','qeqewqwe',job_name=job_name)

        # print('jenkins_conf',jenkins_conf)
        # jenkins = Jenkins

        for k,v in env_manager.get_env(ctx.obj['ENV'], None).items():
            # print(k,v)
            if k not in ctx.obj['ENV_FILES'].keys():
                env_data['environ'][k] = v

        #
        #

        for name, filepath in ctx.obj['ENV_FILES'].items():
        #     print()
            # print('#### {}: {}'.format(name, filepath))
            with open(filepath, 'rt') as f:
                # print(f.read())
                # print(name,f)
                # identity = f'{env}-{name}'
                env_data['environ_files'][name] = f.read()
                # jenkins.set_secret_file(identity, f, job_name=job_name)
        #         for l in f.readlines():
        #             print(l, end='')


        json_env_data = json.dumps(env_data)

        jenkins_file_id = f'{env}-{step}'
        # # Upload env file to jenkins
        if not dry:
            jenkins.set_secret_text(jenkins_file_id, json_env_data, job_name=job_name)
        else:
            print('Test configure jenkins')
            pprint.pprint(env_data)
        #
        fp = tempfile.NamedTemporaryFile(mode='w+t', suffix='.env', delete=False)
        fp.write(json_env_data)
        print(fp.name)
        
        
        _config = json.loads(json_env_data)
        for varname,content in _config['environ_files'].items():
            # print(varname,content)
            fp = tempfile.NamedTemporaryFile(mode='w+t', suffix='.env', delete=False)
            fp.write(content)
            fp.flush()
            print('fp.name',varname,fp.name)




@wrapper.command(name='config')
# @click.option('--service', '-s', cls=ServicesOption, required=True)
@pass_service
@click.pass_context
def wrapper_config(ctx, service):
    env = ctx.parent.parent.invoked_subcommand
    print('wrapper_config',env,service)


    def _exec_config(step):
        compose_files = ctx.obj['CONFIG'].get_compose_files(env, step)
        compose_file_args = utils.compose_files_to_docker_command_args(compose_files)

        # Crate command
        args = ['docker compose']
        args += compose_file_args
        args += ['config']

        utils.run(args)

    for step in ctx.obj['CONFIG'].dependency_of(service):
        _exec_config(step)

    if service != 'all' or ctx.obj.get('JENKINS_MODE'):
        _exec_config(service)


    # print_env_files()


@click.group(invoke_without_command=False)
@click.option('-q', '--quote', default='never',
              type=click.Choice(['always', 'never', 'auto']),
              help="Whether to quote or not the variable values. Default mode is always. This does not affect parsing.")
@click.pass_context
def conf(ctx, quote):
    env = ctx.parent.parent.invoked_subcommand

    env_files = ctx.obj['CONFIG'].get_env_files(env)
    if len(env_files):
        ctx.obj['FILE'] = env_files[0]

    ctx.obj['QUOTE'] = quote
    
    if not ctx.invoked_subcommand:
        ctx.invoke(conf_list)


@conf.command(name='list')
@click.pass_context
def conf_list(ctx):
    ctx.invoke(_list)


@conf.command(name='set')
@click.pass_context
@click.argument('key', required=True)
@click.argument('value', required=True)
def conf_set(ctx, key, value):
    ctx.forward(_set, key, value)
    click.echo('---------')
    ctx.invoke(conf_list)


@conf.command(name='unset')
@click.pass_context
@click.argument('key', required=True)
def conf_unset(ctx, key):
    ctx.forward(_unset, key)


conf.add_command(conf_set, name='add')
conf.add_command(conf_unset, name='rm')

wrapper.add_command(conf)
