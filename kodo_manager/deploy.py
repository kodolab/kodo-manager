import click
import pprint
from docker import errors

from kodo_manager import utils
from kodo_manager.utils import eval_compose_files


@click.pass_context
def docker_pull_image(ctx, client, image):
    if image:
        try:
            click.secho('docker pull {} '.format(image), fg='bright_cyan', nl=False)
            for line in client.api.pull(image, stream=True, decode=True):
                # click.secho(str(line), nl=True)
                if 'error' in line:
                    raise Exception(line['error'])
                click.secho('.', nl=False)
            click.secho('.', nl=True)

            # print(json.dumps(json.loads(line), indent=4))

            # client.images.pull(image)
            click.secho('Pulled image {}'.format(image),
                        fg='bright_green')

        except errors.NotFound as e:
            click.secho('.', nl=True)
            click.secho('Docker image [{}] Not Found'.format(image), fg='red')
            click.secho('Try to run build command'.format(), fg='yellow')
            ctx.abort()
        except Exception as e:
            click.secho('.', nl=True)
            click.secho('Error during pull image [{}] '.format(image), fg='red')
            click.secho(str(e), fg='yellow')
            ctx.abort()


@click.command()
@click.pass_context
def deploy(ctx, env, service, update=False, isdep=False, nopull=False):
    if not isdep:
        click.secho(f'Start Deploy  [env:{env}] [service:{service}]', fg='green')
        # Pull all images
        if not nopull:
            ctx.invoke(pull, env, service)
    else:
        click.secho(f'deploy  [env:{env}] [service:{service}]')

    if ctx.obj['CONFIG'].is_protected_env(env):
        click.confirm('You are doing a deployment in "{}" \n Do you want to continue?'.format(env), abort=True)

    notification = ctx.obj['CONFIG'].get_notifier(env)
    docker_machine = ctx.obj['CONFIG'].get_docker_machine(env)

    # sys.stdout.flush()
    for dep in ctx.obj['CONFIG'].dependency_of(service):
        """recurse build pass for any dependency"""
        ctx.invoke(deploy, env, dep, update, isdep=True)

    with ctx.obj['DOCKER_MANAGER'].with_docker_machine(docker_machine) as docker_client:

        compose_files = ctx.obj['CONFIG'].get_compose_files(env, service)
        if not compose_files:
            click.secho('skyp [{}] docker update. No compose files'.format(service), fg='yellow')
            return

        union_compose_file = eval_compose_files(compose_files)
        if update:
            if union_compose_file:
                dgroup = ctx.obj['CONFIG'].get_docker_group_name(env)

                for service_name, conf_service in union_compose_file.get('services', {}).items():
                    name = '{}_{}'.format(dgroup, service_name)

                    try:
                        service = docker_client.services.list(filters={'name': name})[0]
                    except:
                        click.secho('Docker service [{}] Not Found'.format(name), fg='red')
                        click.secho('Try to deploy without --update options'.format(), fg='yellow')
                        ctx.abort()
                    #
                    # pprint.pprint(conf_service)
                    # print('############')
                    # pprint.pprint(service.attrs)
                    #
                    # raise Exception('test')
                    service_envs = conf_service.get('environment', {})
                    service_envs = ['{}={}'.format(k, v) for k, v in service_envs.items()]

                    task_template_labels = conf_service.get('labels', {})
                    command = conf_service.get('command', None)
                    print('command',command)

                    current_task_template_labels = service.attrs \
                        .get('Spec', {}) \
                        .get('TaskTemplate', {}) \
                        .get('ContainerSpec', {}) \
                        .get('Labels', {})

                    if 'com.docker.stack.namespace' in current_task_template_labels:
                        task_template_labels['com.docker.stack.namespace'] = current_task_template_labels[
                            'com.docker.stack.namespace']

                    image = conf_service.get('image', None)

                    if not image:
                        raise Exception('Image not found')

                    if service.update(image=image,
                                      env=service_envs,
                                      command=command,
                                      container_labels=task_template_labels,
                                      # labels=service_labels,
                                      force_update=True
                                      ):

                        click.secho('Service  {}:{} updated!'.format(name, image),
                                    fg='bright_green')
                    else:
                        click.secho('Service  {}:{} updated Failed!'.format(name, image), fg='red')
                        click.secho('Try to deploy without --update options'.format(), fg='yellow')
                        ctx.abort()

            notification.deploy(env, service)
            return
        else:

            compose_files = ctx.obj['CONFIG'].get_compose_files(env, service)
            if compose_files:
                # utils.run(['env'])
                # print('compose_files',compose_files)
                docker_group_name = ctx.obj['CONFIG'].get_docker_group_name(env)
                compose_file_args = utils.compose_files_to_docker_command_args(compose_files,
                                                                               opt_name='--compose-file')

                vargs = ['docker', 'stack', 'deploy']
                vargs += ['--with-registry-auth']
                # vargs += ['--resolve-image', 'changed']
                vargs += compose_file_args
                vargs += [docker_group_name]

                # print(vargs)

                res = utils.run(vargs)
                if res != 0:
                    click.secho('Error during deploy for env: {} step:{}'.format(env, service), fg='red')
                    ctx.abort()

            notification.deploy(env, service)


@click.pass_context
def pull_step(ctx, env, service, docker_client):
    """
    Pull image
    """
    compose_files = ctx.obj['CONFIG'].get_compose_files(env, service)
    if not compose_files:
        click.secho('skyp [{}] docker update. No compose files'.format(service), fg='yellow')
        return
    union_compose_file = eval_compose_files(compose_files)
    for service_name, conf_service in union_compose_file.get('services', {}).items():
        image = conf_service.get('image', None)
        docker_pull_image(docker_client, image)


@click.pass_context
def pull(ctx, env, service):
    utils.docker_login()
    deps = ctx.obj['CONFIG'].dependency_of(service)
    click.secho(f'Pull all images for  [env:{env}] [service:{service}] and deps:{deps}', fg='green')

    docker_machine = ctx.obj['CONFIG'].get_docker_machine(env)

    with ctx.obj['DOCKER_MANAGER'].with_docker_machine(docker_machine) as docker_client:

        if ctx.obj.get('JENKINS_MODE'):
            ctx.invoke(pull_step, env, None, docker_client)
        else:
            for dep in deps:
                """recurse build pass for any dependency"""
                ctx.invoke(pull_step, env, dep, docker_client)
            ctx.invoke(pull_step, env, service, docker_client)


@click.pass_context
def get_image_name_step(ctx, env, service, ):
    """
    Pull image
    """
    compose_files = ctx.obj['CONFIG'].get_compose_files(env, service)
    if not compose_files:
        return
    union_compose_file = eval_compose_files(compose_files)
    for service_name, conf_service in union_compose_file.get('services', {}).items():
        image = conf_service.get('image', None)
        click.secho(f'IMGNAME: {env} {service} {image}')
        # docker_pull_image(docker_client, image)


@click.pass_context
def imageNameCommand(ctx, env, service):
    deps = ctx.obj['CONFIG'].dependency_of(service)

    if ctx.obj.get('JENKINS_MODE'):
        ctx.invoke(get_image_name_step, env, None, )
    else:
        for dep in deps:
            """recurse build pass for any dependency"""
            ctx.invoke(get_image_name_step, env, dep)
