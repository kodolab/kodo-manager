#!/usr/bin/env python
import logging
import os
import sys
import pprint

import importlib.machinery
import click
from kodo_manager.develop import develop
from kodo_manager.wrapper import wrapper
from kodo_manager.config import ConfigManager, JenkinsConfigManager
from kodo_manager.docker_manager import DockerManager

# Get an instance of a logger
logger = logging.getLogger(__name__)


class ConfGroupWrapper(click.Group):
    jenkins_env = None

    # def __call__(self, *args, **kwargs):
    #     """Alias for :meth:`main`."""
    #     if self.ctx.obj.get('JENKINS_MODE'):
    #         return wrapper.main(*args, **kwargs)
    #     return self.main(*args, **kwargs)

    # @staticmethod
    def load_conf(self,ctx):

        jenkins_env = ctx.params.get('jenkins')
        if jenkins_env:
            click.echo('Starting jenkins mode (configuration expected by env)', color=ctx.color)
            ctx.obj['CONFIG'] = JenkinsConfigManager(jenkins_env)
            ctx.obj['ENV'] = jenkins_env
            self.jenkins_env = jenkins_env
            ctx.obj['JENKINS_MODE'] = True
            return


        conf = ctx.params.get('conf')
        if conf:
            try:

                MODULE_PATH = conf
                MODULE_NAME = "conf_loader"

                PKG_PATH = os.path.join(os.path.dirname(MODULE_PATH), '__init__.py')
                PKG_NAME = 'curr_project'

                spec = importlib.util.spec_from_file_location(PKG_NAME, PKG_PATH)
                pkg = importlib.util.module_from_spec(spec)
                sys.modules[spec.name] = pkg

                spec = importlib.util.spec_from_file_location(MODULE_NAME, MODULE_PATH)
                module = importlib.util.module_from_spec(spec)
                module.__package__ = PKG_NAME
                spec.loader.exec_module(module)
                ctx.obj['CONFIG'] = ConfigManager(module.CONF, base_path=os.path.dirname(MODULE_PATH))

                return
            except FileNotFoundError:
                error = 'Error: Invalid value for "-c" / "--conf": Could not open file: {}: No such file or directory'.format(
                    conf)
                click.echo(error, color=ctx.color)
                ctx.exit()
                return

        if os.path.exists(os.path.join(os.getcwd(), 'kodo_manager.py')):
            print('load kodo_manager.py from current dir')
            raise NotImplementedError()

        try:
            from conf import CONF
            # pprint.pprint(CONF)
            ctx.obj['CONFIG'] = ConfigManager(CONF)
            return
        except ImportError as e:
            print('conf.py not found')
            ctx.obj['CONFIG'] = ConfigManager({})
            return

    def init_docker_manager(self, ctx):
        ctx.obj['DOCKER_MANAGER'] = DockerManager()

    def add_envs_command(self, ctx):
        if ctx.params.get('jenkins'):
            jenkins_env = ctx.params.get('jenkins')
            self.add_command(wrapper, jenkins_env)
            return

        for k in ctx.obj['CONFIG'].envs():
            if k not in self.commands:
                self.add_command(wrapper, k)
        if ctx.obj['CONFIG'].envs():
            self.add_command(develop, 'dev')

    def make_context(self, info_name, args, parent=None, **extra):
        initial_args = args
        args = [x for x in args if x not in ('--help')]

        initial_no_args_is_help = self.no_args_is_help
        initial_invoke_without_command = self.invoke_without_command
        self.no_args_is_help = False
        self.invoke_without_command = True
        ctx = super(ConfGroupWrapper, self).make_context(info_name, args, parent, **extra)

        self.no_args_is_help = initial_no_args_is_help
        self.invoke_without_command = initial_invoke_without_command

        self.load_conf(ctx)
        self.init_docker_manager(ctx)
        self.add_envs_command(ctx)

        # args = [x for x in args if x not in ('--jenkins')]

        with ctx.scope(cleanup=False):
            self.parse_args(ctx, args)

        return ctx

    def resolve_command(self, ctx,  *args, **kwargs):
        if ctx.obj.get('JENKINS_MODE'):
            """
            se lancio jenkins setto l;envairnoment e indico il servizio di default (all)
            """
            print('resolve_command in jankis mode')

            args = [self.jenkins_env] + args[0] + ['-s', 'all']

            cmd_name, cmd, args = super(ConfGroupWrapper, self).resolve_command(ctx, args, **kwargs)
            return cmd_name, cmd, args
        else:
            return super(ConfGroupWrapper, self).resolve_command(ctx,  *args, **kwargs)


    def invoke(self, ctx):
        return super(ConfGroupWrapper, self).invoke(ctx)

        try:
            return super(ConfGroupWrapper, self).invoke(ctx)
        except click.UsageError as e:
            click.echo(ctx.get_help(), color=ctx.color)
            ctx.exit()




@click.group(invoke_without_command=False, cls=ConfGroupWrapper)
@click.option('-j', '--jenkins')
@click.option('-c', '--conf')
@click.pass_context
def cli(ctx, conf, jenkins):
    pass


# cli.add_command(develop, name='dev')

# todo: aggiungere il comando "create" per crare una nuova configurazione  ~/.kodo_manager/<config>/conf.py ~/.kodo_manager/<config>/<envs>/prod.env
# todo: aggiungere il comando "setup" cerca tutte le configurazioni in  ~/.kodo_manager/<config..> e per ognuna genera un'alias  dentro ~/.kodo_manager/kodorc
# todo: avviso di aggiungere  "source  ~/.kodo_manager/kodorc" al proprio .bashrc o .zshrc
# todo: in questo modo sara possibile chiamare direttamente jk = km -c ~/.kodo_manager/km/conf.py
def main():
    cli(obj={})



if __name__ == '__main__':
    main()

